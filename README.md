# Krater

A gopher framework for lua.

## Im sold, how do I install it?
It is really elementary my dear watson
```sh
#you may need to install luasocket using your package manager: 
#http://w3.impa.br/~diego/software/luasocket/installation.html
#get into your lua directory
cd /usr/local/share/lua/5.3
#download krater
git clone https://gitlab.com/jhumphrey/krater
```
Now you can require it:
```lua
require "krater"
```
## How do I use it?
Let's start with an exampe, and then explain it.
```lua
-- load all libraries you want to use
require "krater" -- and naturally this is all you really want to use
myFirstKrater = {
    default = {
        {"i","Welcome to my first Krater!"},
        {"1", "Come on in!", "/greet"},
        {"1", "Read War and Peace", "/warandpeace.txt"} -- of course you have war and peace in your project directory.
    },
    map = {
        ["/greet"] = function(args)
            return "Hello, World!\r\n.\r\n"
        end,
        ["/warandpeace%.lua"] = handle_text
    }
}
krater(myFirstKrater)
```
Okay, let's explain it all
The gopher hole is mapped as a table.
the table has a `map` entry. This entry is a table of functions for paths.
For example suppose I wanted to handle each entry containing emacs. I could add
```lua
["emacs"] = function(a) return "ed(1) is the standard editor!\r\n.\r\n" end
```
(RFC requires a period on a blank line by itself to terminate text communication.)
All requests for resources containing `emacs` would display `ed(1) is the standard editor!`
There are some functions for your convience. handle_text is one of them. It will fetch a .txt file from the specified path.
The other part `default` is a gopher map kinda like index.html it is the landing page of your hole.
this is an array of entries corresponding to entries in the actual gopher map.
Each entry has the following attributes:

1. `type`: The type number (see RFC 1436) (default "i")
2. `title`: the displayed title of the entry. (default "????") 
3. `path`:the path to the resource. (kinda like the URL) (default "/")
4. `server`: the IP address or hostname of the server this links to. (default configured hostname.)
5. `port`: the port the server is on. (default 70) 

You can specify these by key, or you can do this as an array (in the order listed of course).
