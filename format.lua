----------------------------------------------------------------------
-- various formats for gopher menus
-- gph2tab: convert gph to table format. 
-- useful for automapping a gopher menu.

function gph2tab(txt)
	local tab = {}
	-- go through line by line.
	for line in txt:gmatch("^[\14\10]+") do
		-- does the line begin with a [?
		if line:sub(1,1) == "[" then
			-- obtain the results
			local _type,title,path,serv,port = line:match("[(.-)|(.-)|(.-)|(.-)|(.-)]")
			if _type then
				table.insert(tab,{_type,title,path,serv,port})
			end
		else
			-- we are an informational entry.
			table.insert(tab,{"i",line,"(NULL)","0.0.0.0","70"}) --TODO: do we want the actual hostname?
		end
	end
	return tab
end

-- gph2menu: convert to final format. Useful for predefined handlers
function gph2menu(txt)
	local tab = {}
	-- go through line by line.
	for line in txt:gmatch("^[\14\10]+") do
		-- does the line begin with a [?
		if line:sub(1,1) == "[" then
			-- obtain the results
			local _type,title,path,serv,port = line:match("[(.-)|(.-)|(.-)|(.-)|(.-)]")
			if _type then
				table.insert(tab,table.concat({_type..title,path,serv,port},"\t"))
			end
		else
			-- we are an informational entry.
			table.insert(tab,table.concat({"i"..line,"(NULL)","0.0.0.0","70"},"\t")) --TODO: do we want the actual hostname?
		end
	end
	return table.concat(tab,"\14\10") 
end

-- menu2tab: convert a menu to table for incomprehensable reasons
function menu2tab(txt)
	tab = {}
	-- go through the menu line by line
	for line in txt:gmatch("^[\14\10]+") do
		local _type,title,path,serv,port = line:match("(.)(.-)\t(.-)\t(.-)\t(.-)")
		table.insert(tab,{_type,title,path,serv,port})
	end
	return tab
end

-- tab2menu: convert a table to a menu
function tab2menu(menu,ip,port)
	-- each pair consists of type, title, path, server, port
	local formatted_lines = {}
	for i, line in ipairs(menu) do
		formatted_lines[i] = table.concat({(line.type or line[1] or "i")..(line.title or line[2] or "????"),line.path or line[3] or "/", line.server or line[4] or ip or menu.ip or "0.0.0.0", line.port or line[5] or port or menu.port or "70"},"\t")
	end
	return table.concat(formatted_lines,"\13\10").."\13\10.\13\10"
end

--
-- Quirks in the code possibly due to: https://www.youtube.com/watch?v=U3f_-rJQ_Os or https://www.youtube.com/watch?v=QKCeapAxYzc
-- https://invidious.tube/watch?v=6p_G_S4jU4o&autoplay=1&listen=true&list=PLb9kr1UxWscDJHl8HQkoK_ehbZS-RZR50&index=14
-- https://librivox.org/the-brothers-karamazov-by-fyodor-dostoyevsky/
-- Me
