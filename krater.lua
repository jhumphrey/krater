-- get the socket library.
local socket = require "socket"

require "format"

function krater(hole)
local server = socket.bind("*",70)
ip,port = server:getsockname()

ip = "localhost"

hole = hole or {default=require("default_hole")}

-- loop to handle clients
while true do
	local client = server:accept()
	client:settimeout(30) -- this client has to get his request within 30 seconds.
	local clientip,clientport = client:getpeername()
	if clientip then
		print("REQUEST: from: "..clientip.." port "..(clientport or "UNKNOWN"))
	end
	local line, err = client:receive()
	if err then
		io.write("ERROR: ")
		io.write(err)
		io.write("\n")
	else
		-- here is the main program
		-- parse request
		-- are we an empty line?
print("client request is "..line)
		if line == "" then
			client:send(tab2menu(hole.default,ip,port))
			--client:send(".\13\10")
print("RESPONSE: sent "..tab2menu(hole.default))
		else
			-- check our hole for function mappings. These will return the content
			-- go through the entire gophermap looking for matches.
			for str, func in pairs(hole.map) do
				local result = line:match("^"..str) -- TODO: anchor may not be neccesary
				if result then
					client:send(func(result))
					break
				end
			end
		end
	end
	client:close()
end
end

function getport()
	return port or 70
end

function gethostname()
	return ip or "0.0.0.0"
end

-- Content Handler functions

-- handle type 0
function handle_text(what)
	return handle_bin(what).."\13\10.\13\10"
end

-- handle type 1
function handle_gph(what)
	local menu = handle_bin(what)
	if menu then
		return gph2menu(what).."\13\10.\13\10"
	end
end

-- handle type 2 CSO? 
-- handle type 3 error
function handle_error(what)
	return what or "An error occured, and the admin won't tell you what it is.\r\n.\r\n"
end

-- TODO: handle type 4 Mac HexEncoded file.

-- handle type 5 and type 9
function handle_bin(what)
	-- evaluate the .. beforehand
	path = choppath(prepare_path(what))
	-- open the file if you have permission.
	local f,e = io.open(path)
	if e then
		return ""
	end
	local str = f:read("*all")
	f:close()
	return str
end

-- TODO: handle type 6 uuencoded file
-- TODO: do we want to handle type 7 natively?
-- We will not handle type 8 telnet natively
-- most types can use the handle_bin extension

-- section for taking care of urls.

function prepare_path(path)
	-- split the path up into sections. avoiding the .. attack
	local i,previ = 0,0
	local urltab = {}
	while i < #path do
		i = path:find("/",previ) or -1
			local chunk = path:sub(previ,i-1)
		if i == -1 then 
			chunk = path:sub(previ,-1) 
		end
			if chunk == "." then
				-- do nothing
			elseif chunk == ".." then
				--remove previous item from table.
				table.remove(urltab)
			else
				-- TODO: add more test cases!
				table.insert(urltab,chunk)
			end
		previ = i+1
		if i == -1 then break end
	end
	return table.concat(urltab,"/")
end

-- chop of the leading slash
function choppath(path)
	if path:sub(1,1) == "/" then
		return choppath(path:sub(2,-1)) --make it recursive to avoid //etc/passwd or such like
	end
	return path
end
